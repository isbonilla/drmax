#!/usr/bin/env bash

#Set c or -c or --component as second parameter to specifies that we don't wan't to create a block but a component

FOLDER='blocks/'
HTMLELEMENT='section'

if [[ $2 == 'c' || $2 == '-c' || $2 == '--component' ]] ; then
  FOLDER='components/'
  HTMLELEMENT='div'
fi

DIR="$(cd "$(dirname "$0")" && pwd)"
SRC_PATH=$DIR'/../src/'
BLOCKS_PATH=$SRC_PATH$FOLDER

NAME_NEW_BLOCK=$1
NEW_FOLDER_PATH=$BLOCKS_PATH$NAME_NEW_BLOCK/
CLASS_NEW_BLOCK=`echo $NAME_NEW_BLOCK | tr '[:upper:]' '[:lower:]'`

if ! [ -d "$NEW_FOLDER_PATH" ]; then
  mkdir $NEW_FOLDER_PATH
  touch $NEW_FOLDER_PATH$NAME_NEW_BLOCK'.vue'

  {
    echo '<template>'
    echo '    <'$HTMLELEMENT' class="'$CLASS_NEW_BLOCK'">'
    echo '        <Fonts text="Put your content here" />'
    echo '    </'$HTMLELEMENT'>'
    echo -e '</template>\n'

    echo '<script src="./'$NAME_NEW_BLOCK'.js"></script>'
    echo '<style scoped lang="scss" src="./'$NAME_NEW_BLOCK'.scss"></style>'
  } >> $NEW_FOLDER_PATH$NAME_NEW_BLOCK'.vue'

  {
    echo -e "'use strict'\n"

    echo 'export default {'
    echo "    name: '$NAME_NEW_BLOCK'"
    echo '}'
  } >> $NEW_FOLDER_PATH$NAME_NEW_BLOCK'.js'

  {
    echo -e '.'$CLASS_NEW_BLOCK' {\n'
    echo '}'
  } >> $NEW_FOLDER_PATH$NAME_NEW_BLOCK'.scss'

  awk '1;/new blocks/{ print "import '$NAME_NEW_BLOCK' from '\''@/'$FOLDER$NAME_NEW_BLOCK'/'$NAME_NEW_BLOCK'.vue'\''"}' $SRC_PATH'App.js' > $SRC_PATH'App2.js'
  mv $SRC_PATH'App2.js' $SRC_PATH'App.js'

  awk '1;/components: {/{ print "        '$NAME_NEW_BLOCK',"}' $SRC_PATH'App.js' > $SRC_PATH'App2.js'
  mv $SRC_PATH'App2.js' $SRC_PATH'App.js'

  awk '/end app__container/{ print "                <'$NAME_NEW_BLOCK' />"}1;' $SRC_PATH'App.vue' > $SRC_PATH'App2.vue'
  mv $SRC_PATH'App2.vue' $SRC_PATH'App.vue'

  echo "The Block has been created."

else
  echo "This Block already exists."
fi
