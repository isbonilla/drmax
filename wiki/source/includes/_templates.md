# Dynamic Templates

We did some dynamic templates to manage **Landing Pages** and **LiveStreams**.

If you need to access manually to the configuration, you can do it by using Another Redis Desktop Manager.
You'll find further information in that document : https://docs.google.com/document/d/1HUoAjBV7rTPDhbhnukcknh3GnySFZ1SD0ZpjUCfMYFo/edit
