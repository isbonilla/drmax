export const it = {
    consent: {
        txt: [
            'Accedendo a questo contenuto',
            '<b> dichiari di avere almeno 18 anni.</b>'
        ],
        yes: 'Ho più di 18 anni',
        no: 'Ho meno di 18 anni'
    },
    header: {
        title: 'Ci prendiamo cura di te, del tuo benessere e della tua bellezza',
        textscroll: `Scopri l'offerta`
    },
    offer: {
        title: 'OFFERTA ESCLUSIVA VEEPEE',
        subtitle: 'Spedizione gratuita',
        offer_title: '<span class="offer__red">2€</span> di sconto extra ogni<span class="offer__red"> 20€ spesi*</span>',
        offer_subtitle: 'Con il codice'
    },
    staticzone: {
        title: '<span class="staticZone__green">Dr. Max:</span> Ti capisce. Ti consiglia. Ti conviene.',
        subtitle: 'Scegli tra una <span class="staticZone__red">vasta gamma di prodotti,</span> specifici per le tue esigenze e il tuo stile di vita.'
    },
    slider: {
        title: 'Scopri i nostri MUST HAVE',
        subtitle: 'Coccolati con prodotti per la cura del viso, del corpo, dei capelli e tanto altro...',
        card1Title: 'Nuance Micellar Water Sensitive Skin 500 ml',
        card1Subtitle: 'Lenisce, idrata, esfolia e protegge la pelle da germi e da impurità. Adatta anche alla pelle sensibile.',
        card2Title: 'Skinexpert by Dr. Max Shower oil Almond 250 ml',
        card2Subtitle: 'Olio-Doccia alla Mandorla per tutti i tipi di pelle. Deterge e leviga la cute, rilasciando una delicata fragranza.',
        card3Title: 'Skinexpert by Dr. Max ReviHair Gummies 60 Pezzi',
        card3Subtitle: 'Caramelle con vitamine e minerali ideali per ottenere capelli, pelle e unghie sani e forti.',
        card4Title: 'Dr.Max Natural Shampoo with Quinine 400 ml',
        card4Subtitle: 'A base di Chinina e con formulazione al 92% vegetale. Con proprietà rinforzanti ideali per capelli deboli e sfibrati.',
        card5Title: 'Nuance Absolute Caviar And Pearl Eye Cream 15 ml',
        card5Subtitle: 'Contorno occhi illuminante e dall\' effetto tensore che risveglia lo sguardo mantenendo la pelle elastica e morbida.'
    },
    categories: {
        title: 'Esplora per categorie',
        card1: 'Corpo',
        card2: 'Viso',
        card3: 'Capelli',
        card4: 'Integratori'
    },
    reinsurance: {
        card1Title: 'Spedizione gratuita',
        card1Subtitle: 'A partire da 18,99€',
        card2Title: 'Offerte imperdibili',
        card2Subtitle: 'Tutti i giorni',
        card3Title: 'Consegna express',
        card3Subtitle: 'In 24/48 ore della spedizione',
        card4Title: 'Vasto assortimento',
        card4Subtitle: 'Più di 35.000 prodotti'
    },
    staticzone2: {
        title: 'Salute e Benessere alla<br class="no-desktop"> portata di tutti ... 365 giorni all\'anno!',
        subtitle: 'Registrati e approfitta di uno sconto di<br class="no-desktop"> <b>2€</b> ogni <b>20€ di spesa</b>'
    },
    sticky: {
        text: 'Ricevi <span class="sticky__red"> 2€ di sconto</span> ogni <span class="sticky__red">20€ spesi*</span>'
    },
    mention: {
        txt: '*Promozione valida dal 14/01/2024 fino al 21/01/2024. L’extra sconto esclusivo si applica progressivamente in base al valore del carrello (ad esempio su una spesa di 40€ lo sconto è di 4€, su 60€ lo sconto è di 6€, etc). La promozione è valida su tutto il catalogo esclusi i prodotti appartenenti alla categoria “Medicinali”. Per applicare il codice è necessario iscriversi al sito e aver effettuato il log-in. Il codice VPMAX non è cumulabile con altri codici coupon attivi.'
    }

}
