export const lu = {
    consent: {
        txt: [
            'En accédant à ce contenu, ',
            '<b>vous déclarez avoir 18 ans ou plus.</b>'
        ],
        yes: 'J’ai plus de 18 ans',
        no: 'J’ai moins de 18 ans'
    }
}
