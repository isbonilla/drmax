export const es = {
    consent: {
        txt: [
            'Accediendo a este contenido ',
            '<b>declaras tener 18 años o más.</b>'
        ],
        yes: 'Tengo más de 18 años',
        no: 'Tengo menos de 18 años'
    }
}
