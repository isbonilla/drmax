export const en = {
    consent: {
        txt: [
            'By accessing this content, ',
            '<b>you declare that you are at least 18 years old.</b>'
        ],
        yes: 'I am over the age of 18 ',
        no: 'I am under the age of 18 '
    }
}
