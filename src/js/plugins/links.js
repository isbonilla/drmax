import Vue from 'vue'
import common from '../conf/common'
import linkHome from '../conf/linkHome'
import detectEnv from '../conf/detectEnv'
import lang from '../conf/lang'

const getLink = (type, options = {}) => {
    const app = lang.site === 'privalia' ? 'privalia' : (lang.isVex) ? 'vexapp' : 'appvp'
    const operationId = options.operationId ? options.operationId : common.operation.id
    const operationCode = options.operationCode ? options.operationCode : common.dumbo.operationCode
    const universeId = options.universeId ? options.universeId : common.operation.universeID
    const isApp = options.app ? options.app : detectEnv.isApp
    const isPreview = (process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'dumbopp') && detectEnv.isDesktop

    switch (type) {
    case 'marketplaceHome':
        if (isApp) {
            return `${app}://operation/bpt${operationId}`
        } else {
            return `https://${lang.hostname}/gr/catalog/bpt${operationId}`
        }

    case 'marketplaceCatalog':
        if (isApp) {
            return `${app}://specialcatalog/bpt${operationId}`
        } else {
            return `https://${lang.hostname}/gr/catalog/bpt${operationId}`
        }

    case 'marketplaceProduct':
        if (isApp) {
            return `${app}://fp/bpt${operationId}/0/bppf${options.productId}`
        } else {
            return `https://${lang.hostname}/gr/product/bpt${operationId}/bppf${options.productId}`
        }

    case 'marketplaceBranch':
        if (isApp) {
            return `${app}://fp/bpt${operationId}/bpt${operationId}-${universeId}`
        } else {
            return `https://${lang.hostname}/gr/catalog/bpt${operationId}/bpt${operationId}-${universeId}`
        }

    case 'home':
        const key = options.home?.slug || options.home?.id || ''
        const home = linkHome.filter(item => parseInt(item.id) === parseInt(key) || item.slug === key)[0]
        if (isApp) {
            return `${app}://home/${home.id}`
        } else {
            return `https://${lang.hostname}/gr/home/${home.slug}`
        }

    case 'sale':
        if (isApp) {
            return `${app}://operation/${operationId}`
        } else {
            return isPreview ? `http://preview_bo/StartHome/VP5/?operationId=${operationId}` : `https://${lang.hostname}/gr/catalog/${operationId}`
        }

    case 'saleDestin':
        if (isApp) {
            return `${app}://operation/${operationId}`
        } else {
            return `https://${lang.hostname}/web/catalog/v1/sale/${operationId}/redirect`
        }

    case 'universe':
        if (isApp) {
            return `${app}://fp/${operationId}/${universeId}`
        } else {
            return isPreview ? `http://preview_bo/StartHome/VP5/?operationId=${operationId}&selectedUniverseId=${universeId}&src=Spt` : `https://${lang.hostname}/gr/catalog/${operationId}/${universeId}`
        }

    case 'product':
        if (isApp) {
            return `${app}://fp/${operationId}/${universeId}/${options.productId}`
        } else {
            return isPreview ? `http://preview_bo/Catalog/FicheProduit/?operationId=${operationId}&universeId=${universeId}&productFamilyId=${options.productId}&src=Spt&imageSource=Auto&lang=fr-FR` : `https://${lang.hostname}/gr/product/${operationId}/${options.productId}`
        }

    case 'filter':
        let filter = options.filter.split('&')
        let filterApp = []
        filter.forEach(f => {
            const filterValue = f.split('=')[1]
            filterApp.push(filterValue)
        })

        if (isApp) {
            // TODO : This only shows the sale but the articles are not filtered
            return `${app}://specialcatalog/${operationId}/${filterApp.join('/')}`
        } else {
            // return `https://${lang.hostname}/ns/fr-fr/operation/${operationId}/specialevent/${options.universeId}/catalog?${filter.join('&')}`
            return `https://${lang.hostname}/gr/catalog/${operationId}?${options.filter}`
        }

    case 'subscribe':
        if (isApp) {
            return detectEnv.isIOS ? `${app}://subscribebrand/${operationId}` : `${app}://subscribebrand/${operationCode}`
        } else if (detectEnv.isMobile) {
            return `https://m.veepee.fr/w2/index.html#subscribebrand/${operationCode}`
        } else {
            return `https://${lang.hostname}/ns/fr-fr/home?subscribebrand=${operationCode}`
        }

    case 'registration':
        return `https://${lang.hostname}/gr/registration?returnurl=${options.returnUrl}`

    case 'authentication':
        return `https://${lang.hostname}/gr/authentication?returnurl=${options.returnUrl}`

    case 'app':
        return `${app}://openwv/${encodeURIComponent(options.href)}`

    case 'sponsor':
        const brand = options.brand ? options.brand : ''
        if (isApp) {
            return `${app}://sponsor/${brand}`
        } else {
            return `https://${lang.hostname}/gr/sponsorship/${brand}`
        }

    case 'direct':
        if (options.browser && isApp) {
            return `${app}://browser/${encodeURIComponent(options.href)}`
        } else {
            return options.href
        }

    default:
        return '#'
    }
}

Vue.prototype.$getLink = getLink
