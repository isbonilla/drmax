import Vue from 'vue'
import VueI18n from 'vue-i18n'
import lang from '@/js/conf/lang'
import store from '@/store/'

// Import language files here
import * as locales from './locales'

store.dispatch('dumbo/setLinks', locales[lang.locale].link)
/* TODO later when it will be ready
Vue.prototype.$media = locales[lang.locale].media
Vue.prototype.$style = locales[lang.locale].style */

let messages = {
    [lang.locale]: locales[lang.locale]
}

if (lang.locale !== 'fr') messages['fr'] = locales['fr']

Vue.use(VueI18n)
export default new VueI18n({
    locale: lang.locale,
    fallbackLocale: 'fr',
    messages: messages
})
