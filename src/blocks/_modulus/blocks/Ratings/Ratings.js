'use strict'

export default {
    props: {
        /**
         * The rating from 0 to 5. You can use half values such as 4.5
         */
        ratings: {
            type: Number,
            default: 5
        },

        /**
         * Update the filled star icon using icomoon icon.
         */
        filledIconClass: {
            type: String,
            default: 'icon-star-filled'
        },

        /**
         * Update the half filled star icon using icomoon icon.
         */
        halfFilledIconClass: {
            type: String,
            default: 'icon-star-half-filled'
        }
    },
    computed: {
        maxStars() {
            return Math.ceil(this.ratings)
        },
        filledStars() {
            return Math.floor(this.ratings)
        }
    },
    methods: {
        showHalfStar(index) {
            return this.ratings - this.filledStars >= 0.5 && index === this.filledStars
        }
    }
}
