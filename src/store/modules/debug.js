'use strict'

const state = () => ({
    showDebug: false,
    showPannel: true,
    showLabelTracking: true,
    showHref: true
})

const actions = {
    setDebug: context => context.commit('setDebug'),
    reversePannelBool: context => context.commit('reversePannelBool'),
    reverseLabelTrackingBool: context => context.commit('reverseLabelTrackingBool'),
    reverseHrefBool: context => context.commit('reverseHrefBool')
}

const mutations = {
    setDebug: state => (state.showDebug = window.location.hash === '#debug' && process.env.NODE_ENV !== 'production'),
    reversePannelBool: state => {
        state.showPannel = !state.showPannel
    },
    reverseLabelTrackingBool: state => {
        state.showLabelTracking = !state.showLabelTracking
    },
    reverseHrefBool: state => {
        state.showHref = !state.showHref
    }
}

export default {
    namespaced: true,
    state,
    actions,
    mutations
}
