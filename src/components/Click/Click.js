'use strict'

import { createNamespacedHelpers } from 'vuex'

const { mapState } = createNamespacedHelpers('debug')
const { mapState: mapStateLoader } = createNamespacedHelpers('loader')

/**
 * Displays a link, a button, or a block element that fires a tracking event on click.<br />
 * Click is globally loaded, so you don't need to import it when you create a new block.<br />
 * Click has a slot so you can set a custom element.
 */
export default {
    name: 'Click',
    props: {
        /**
         * Id of the Click in DUMBO.<br />
         * Used to get the button label and tracking props.
         */
        id: {
            type: String
        },
        /**
         * Define the button size.<br />
         * Available values :
         * <ul>
         *   <li>`xs`</li>
         *   <li>`s` <i>(default value)</i></li>
         *   <li>`m`</li>
         * </ul>
        */
        size: {
            type: String,
            default: 's',
            validator: (value) => {
                return ['xs', 's', 'm'].indexOf(value) !== -1
            }
        },
        /**
         * Define the style of the button.<br />
         * The `submit` variant is not tracked by default.<br />
         * Available values :
         * <ul>
         *   <li>`primary` <i>(default value)</i></li>
         *   <li>`secondary`</li>
         *   <li>`link`</li>
         *   <li>`submit`</li>
         * </ul>
        */
        variant: {
            type: String,
            default: 'primary',
            validator: (value) => {
                return ['primary', 'secondary', 'link', 'submit'].indexOf(value) !== -1
            }
        },
        /**
         * Display the link as a Button component.<br />
         * Has no effect if a slot is specified or if `submit` variant is specified.
         */
        button: {
            type: Boolean
        },
        /**
         * Center the button with margin auto.
         */
        center: {
            type: Boolean
        },

        /**
         * Add classes easy to update.
         */
        buttonClass: {
            type: String
        },
        /**
         * Disabled button isn't tracked by default.
         */
        disabled: {
            type: Boolean
        },
        /**
         * You can pass an svg in the slot so you can change its color with scss variable.<br/>
         * Set the `ìcon property to svg` and use #svg slot (see example below)
         */
        icon: {
            type: String
        },
        /**
         * Switch the icon with the label
         */
        reverse: {
            type: Boolean
        },
        /**
         * Set the button width to 100%.
         */
        block: {
            type: Boolean
        },
        /**
         * Set the label that will show on the Mixpanel dashboard and the text of the created link/button (if no slot specified).<br />
         * The label is not needed if the `id` property is specified.<br />
         * The label will override DUMBO's label even if `id` is specified.
         */
        label: {
            type: [String, Number]
        },
        trackingLabel: {
            type: [String, Number]
        },
        /**
         * Set the type of the click so it sends the right event.
         * Available values :
         * <ul>
         *   <li>`click` <i>(default value)</i></li>
         *   <li>`tab`</li>
         *   <li>`live`</li>
         *   <li>`storeLocator`</li>
         *   <li>`brandAlert`</li>
         * </ul>
         */
        type: {
            type: String,
            default: 'click',
            validator: (value) => {
                return ['click', 'tab', 'live', 'storeLocator', 'brandAlert'].indexOf(value) !== -1
            }
        },
        /**
         * Overwrite the default `redirectionType` set in DUMBO config.
         * Available values :
         * <ul>
         *   <li>`partner` (Website of the brand)</li>
         *   <li>`flashsales` (Flashsales)</li>
         *   <li>`brandsplace` (Brandsplace)</li>
         *  <li>`file` (Docs ex: rules.pdf)</li>
         *  <li>`home` (Home Veepee)</li>
         * </ul>
         */
        redirectionType: {
            type: String,
            validator: (value) => {
                return ['partner', 'brandsplace', 'flashsales', 'file', 'home'].indexOf(value) !== -1
            }
        },
        /**
         * Set the tracking parameters associated with the type entered.<br />
         * List of keys that you may use :
         * <ul>
         *   <li>`city` (<b>storeLocator</b> type) : can be the postal code or the city name</li>
         * </ul>
         */
        trackingParams: {
            type: Object
        },
        /**
         * Set the redirection if needed.<br />
         * The href is not needed if the `id` property is specified.<br />
         * The href will override DUMBO's redirection even if `id` is specified.
         */
        href: {
            type: String
        },
        /**
         * Prevents the button from being tracked.
         */
        noTracking: {
            type: Boolean
        }
    },
    computed: {
        ...mapStateLoader([
            'contentInitialized'
        ]),
        ...mapState([
            'showDebug',
            'showLabelTracking',
            'showHref'
        ]),
        buttonPosition() {
            return (this.center) ? 'click-center' : ''
        },
        isRedirectedOutside() {
            return (this.dataHref && this.dataHref !== '#' && this.redirectionType !== 'home') || false
        },
        fullRedirectionType() {
            if (!this.isRedirectedOutside) return ''
            if (this.redirectionType) {
                switch (this.redirectionType) {
                case 'partner':
                    return 1
                case 'brandsplace':
                    return 2
                case 'flashsales':
                    return 3
                case 'home':
                    return 4
                }
            } else {
                return this.$store.state.dumbo.config.redirectionTypeId
            }
        },
        dumboLink() {
            return (this.$store.state.dumbo.links && this.id) ? this.$store.state.dumbo.links[this.id] : null
        },
        dataButtonLabel() {
            // label prop has priority
            if (this.label) return this.label
            if (!this.label && this.type === 'live') return this.$store.state.livestream.btnLabel

            else if (this.dumboLink) {
                // Dumbo's button label
                if (this.dumboLink.label) return this.dumboLink.label

                // Fallback to Dumbo's tracking label
                else return this.dumboLink.trackingLabel
            }

            return 'label'
        },
        dataTrackingLabel() {
            // trackingLabel prop has priority
            if (this.trackingLabel) return this.trackingLabel

            // Dumbo's tracking label
            else if (this.dumboLink) return this.dumboLink.trackingLabel

            // No label = error
            else {
                console.error('Error: missing tracking label', this.$el)
                return ''
            }
        },
        dataHref() {
            // label prop has priority
            if (this.href) return this.href

            // Dumbo's tracking label
            else if (this.dumboLink) {
                if (this.dumboLink.config.type === 'tracking') {
                    // Tracked element without redirection
                    return null
                } else {
                    // Redirection
                    return this.$getLink(this.dumboLink.config.type, this.dumboLink.config.options)
                }
            } else {
                return null
            }
        },
        dataTarget() {
            if (this.dumboLink && this.dumboLink.config.options.target) return this.dumboLink.config.options.target
            else return '_self'
        }
    },
    methods: {
        trackingHandler() {
            if (this.disabled || this.noTracking || this.variant === 'submit') return

            switch (this.type) {
            case 'click':
                this.$tracking.click(this.dataTrackingLabel, this.isRedirectedOutside, this.fullRedirectionType)
                break
            case 'live':
                this.$tracking.live(this.dataTrackingLabel, this.$store.state.livestream.liveStatusTracking)
                break
            case 'storeLocator':
                this.$tracking.storeLocator(this.trackingParams.city)
                break
            case 'brandAlert':
                this.$tracking.brandAlert()
                break
            default:
                console.error('Tracking Error Bad Type', this.$el)
            }
        }
    }
}
