'use strict'

import { createNamespacedHelpers } from 'vuex'
import Tel from '@/components/_mods/Form/Tel/Tel.vue'
import 'vue-tel-input/dist/vue-tel-input.css'
import PostalCode from '@/components/_mods/Form/PostalCode/PostalCode.vue'
const { mapState } = createNamespacedHelpers('loader')

export default {
    name: 'Collect',
    components: {
        Tel,
        PostalCode
    },
    data: () => ({
        isValid: {},
        status: '',
        errorMessage: '',
        formSettings: {},
        formData: {
            recipient: null,
            collectId: null,
            isOptin: false,
            data: {}
        }
    }),
    props: {
        /**
         * The name of the Collect. Only use this if your operation contains several collects.
         */
        name: {
            type: String,
            default: 'default'
        },

        /**
         * Name of the email to send when the form is submitted. The name must be specified in the mail-api project.<br />
         * For more information, check the <a href="#">wiki</a>.
         */
        sendEmail: {
            type: String,
            default: ''
        },

        /**
         * The input type of the form.<br />
         * All values available :
         * <ul>
         *     <li>`Email` <i>(default value)</i></li>
         *     <li>`Phone Number`</li>
         * </ul>
         */
        inputType: {
            type: String,
            default: 'Email',
            validator(value) {
                return ['Email', 'Phone Number'].indexOf(value) !== -1
            }
        },

        /**
         * The optin type of the form.<br />
         * All values available :
         * <ul>
         *     <li>`Newsletter` <i>(default value)</i></li>
         *     <li>`Get more information`</li>
         *     <li>`Call me back`</li>
         * </ul>
         */
        optinType: {
            type: String,
            default: 'Newsletter',
            validator(value) {
                return ['Newsletter', 'Get more information', 'Call me back'].indexOf(value) !== -1
            }
        },

        /**
         * Set specific sizes for any field in the collect.<br />
         * Each field is a property where the key is the name of the field and the value is the size of the field.<br />
         * The size of each field should be an Integer representing the width as a percentage (example: `50` for 50% width).<br />
         * Default size is 100% if not specified.
         */
        sizes: {
            type: Object
        }
    },
    computed: {
        ...mapState([
            'contentInitialized'
        ]),
        allValid() {
            let allValid = true

            for (const fieldName in this.isValid) {
                allValid = allValid && this.isValid[fieldName]
            }

            return allValid
        }
    },
    beforeMount() {
        if (this.contentInitialized) this.getForm()
    },
    methods: {
        getForm() {
            this.formSettings = this.$collects.find(collect => collect.collectName === this.name)
            this.formData.collectId = this.formSettings.collectId

            this.formSettings.dataConfiguration.forEach(field => {
                this.formData.data[field.name] = field.options.defaultValue
            })
        },
        checkRecipient() {
            const regex = new RegExp(this.formSettings.pattern)
            const validEmail = regex.test(this.formData.recipient)
            this.formData.data.email = this.formData.recipient
            return validEmail
        },
        handleValidate(data) {
            const isValid = { ...this.isValid }
            isValid[data.fieldName] = data.isValid
            this.isValid = isValid
        },
        handleForm() {
            // Prevents the form to be submitted multiple times
            if (this.status === 'loading') return

            // Check recipient
            if (!this.checkRecipient()) {
                this.errorMessage = 'Invalid email'
                return
            }

            // Check if all fields are valid
            if (!this.allValid) {
                this.errorMessage = 'One or more fields are invalid'
                return
            }

            // Clear error message, if any
            this.errorMessage = ''

            // Put the form into "loading" state
            this.status = 'loading'

            // Post data to API
            const apiUrl = '/api/mediaform/collect'
            this.$api.post(apiUrl, this.formData)
                .then(result => {
                    let res = result.data
                    if (res.resultCode === 0) {
                        this.status = 'submitted'
                        this.$tracking.form(this.inputType, this.optinType)
                        this.$emit('success', this.formData)
                    } else {
                        this.status = ''
                        this.errorMessage = res.message
                        const error = `${res.resultCode} : ${this.errorMessage}`
                        console.error(error)
                        this.$tracking.error('form', error, apiUrl)
                        this.$emit('error', res)
                    }
                })
                .catch(error => {
                    this.status = ''
                    let data = (error.response) ? error.response.data : null
                    if (data) {
                        this.errorMessage = data.details.message ? data.details.message : data.details
                        let err = `${data.status} : ${this.errorMessage}`
                        console.error(err)
                        this.$tracking.error('form', err, apiUrl)
                    } else {
                        this.$tracking.error('form', error, apiUrl)
                        console.error(error)
                    }
                })
        }
    },
    watch: {
        contentInitialized(value) {
            if (value === true) {
                this.getForm()
            }
        }
    }
}
